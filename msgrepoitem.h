// msgrepoitem.h
// Copyright (C) 2007-2008 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#ifndef _MSGREPOITEM_H
#define _MSGREPOITEM_H

#include <QTreeWidget>
#include <QDomDocument>
#include "msgrepo.h"
#include "toppane.h"
#include "message.h"
#include "summaryitem.h"

class MsgRepoItem: public QObject, public QTreeWidgetItem
{
  Q_OBJECT
  public:
    MsgRepoItem(QTreeWidget *lv, QDomDocument &all_config, MsgRepo *repo,
                TopPane *summary_view);
    MsgRepoItem(QTreeWidget *lv, MsgRepoItem *insert_after, MsgRepo *repo,
                TopPane *summary_view);
    MsgRepoItem(QTreeWidget *lv, const QDomElement &saved_config,
                MsgRepo *repo, TopPane *summary_view);
    virtual ~MsgRepoItem();
    void prepareToShow(KMenu *menu, KActionCollection *actions);
    void prepareToShowMessage(SummaryItem *msg_item);
    MsgRepo *getRepoOfItem() const;
    QDomElement m_config;
  public slots:
    void showSummary();
  private:
    void constructorCommon();
    MsgRepo *m_repo;
    TopPane *m_summary_view;
};

#endif
