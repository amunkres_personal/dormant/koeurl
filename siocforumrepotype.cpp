// siocforumrepotype.cpp
// Copyright (C) 2007-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#include <kgenericfactory.h>
#include <klocale.h>
#include <kaction.h>
#include <kactioncollection.h>
#include <klineedit.h>
#include "siocforumrepotype.h"
#include "siocforumrepo.h"

#include "siocforumrepotype.moc" // This must be here for moc to be run.

K_EXPORT_COMPONENT_FACTORY(siocforumrepotype,
                           KGenericFactory<SIOCForumRepoType>
                             ("siocforumrepotype"))

SIOCForumRepoType::SIOCForumRepoType(QObject *parent,
                                     const QStringList &args):
  m_libname(args.last())
{
  setComponentData(KGenericFactory<SIOCForumRepoType>::componentData());
  setXMLFile("siocforumrepotypeui.rc");
  
  m_parent = static_cast<ProgramWindow *>(parent);
  KAction *a(actionCollection()->addAction(m_libname, this,
                                           SLOT(configureNewRepo())));
  a->setText(args.first());
}

void SIOCForumRepoType::configureNewRepo()
{
  m_dlg = new SIOCForumRepoDialog(m_parent);
  m_dlg->enableButtonApply(false);
  connect(m_dlg, SIGNAL(okClicked()), this, SLOT(createRepo()));
  m_dlg->show();
}

void SIOCForumRepoType::createRepo()
{
  SIOCForumRepo *repo(new SIOCForumRepo(this, m_dlg->m_name->text(),
                                        m_dlg->m_uri->text()));
  if (repo->m_initialized)
    m_parent->insertRepo(repo);
}

MsgRepo *SIOCForumRepoType::createRepo(const QDomElement &config) const
{
  return new SIOCForumRepo(this, config);
}
