// siocforumcommon.h
// Copyright (C) 2008-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#ifndef _SIOCFORUMREPOCOMMON_H
#define _SIOCFORUMREPOCOMMON_H

// #include <librdf.h>
#include <Soprano/QueryResultIterator>
#include <QString>
#include <QUrl>

QUrl getURIValue(const Soprano::QueryResultIterator &results, int index);
QString getLiteralValue(const Soprano::QueryResultIterator &results,
                        int index);
// Currently, nothing uses this function:
// QString getCompositeValue(librdf_query_results *results, int l_index,
//                           int u_index);
QString getConditionalCompositeValue(
  const Soprano::QueryResultIterator &results, int l_index, int u_index_1,
  int u_index_2);

#endif
