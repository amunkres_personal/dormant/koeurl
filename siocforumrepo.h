// siocforumrepo.h
// Copyright (C) 2007-2008 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#ifndef _SIOCFORUMREPO_H
#define _SIOCFORUMREPO_H

#include <kio/job.h>
#include <Soprano/Backend>
#include <Soprano/Model>
#include <Soprano/Parser>
#include <Soprano/QueryResultIterator>
#include <QDomElement>
#include <QDomDocument>
#include <QMap>
#include <QString>
#include <QStringList>
#include "siocforumrepotype.h"
#include "msgrepo.h"
#include "program.h"
#include "siocforummessage.h"

class SIOCForumRepoIterator;

class SIOCForumRepoIterator: public MsgRepo::iterator
{
  public:
    // SIOCForumRepoIterator(librdf_query_results *r);
    SIOCForumRepoIterator(const Soprano::QueryResultIterator &r);
    SIOCForumRepoIterator();
    virtual void operator++();
    virtual bool operator!=(const MsgRepo::iterator &other) const;
    virtual QString getID() const;
    virtual QString getParentID() const;
    virtual QString getField(int index) const;
  private:
    int m_count;
    // librdf_query_results *m_results;
    Soprano::QueryResultIterator m_results;
};

class SIOCForumRepo: public MsgRepo
{
  Q_OBJECT
  public:
    SIOCForumRepo(const SIOCForumRepoType *type, const QString &name,
                  const QString &uri);
    SIOCForumRepo(const SIOCForumRepoType *type,
                  const QDomElement &config);
    virtual QString name() const;
    virtual void summaryFields(QStringList &fields) const;
    virtual QDomElement createConfig(QDomDocument doc) const;
    virtual void beginActivatingRepo(KMenu *menu, KActionCollection *actions);
    virtual void beginActivatingMessage(const QString &id);
    virtual MsgRepo::iterator *begin();
    virtual MsgRepo::iterator *end();
    virtual Message *getSelectedMessage() const;
    bool m_initialized;
  signals:
    void proceed();
  private slots:
    void retrieveMessages();
    void retrievalFinished(KJob *copied);
    // void processIndex(KIO::Job *copied);
    void reload();
  private:
    typedef QMap<QString, int> URLMap;
    typedef QList<KUrl> URLQueue;
    typedef QMap<QString, URLQueue> HostMap;
    typedef QMap<QString, QString> HeaderMap;
    void beginBuildingSummary();
    // bool retrieveMessagesBy(librdf_query *query);
    bool retrieveMessagesBy(const QString &query);
    void retrieveUserData();
    bool enqueueUnvisitedURL(KUrl src_url);
    void beginQueuedRetrievals();
    void retrieveQueuedURL(URLQueue &queue);
    void constructorCommon();
    void loadIndex();
    bool loadIntoModel(KUrl local_url, KUrl orig_url);
    const SIOCForumRepoType *m_type;
    QString m_name, m_uri;
    URLMap m_visited_urls;
    HostMap m_host_queues;
    HeaderMap m_headers;
    // librdf_world *m_world;
    // librdf_storage *m_storage;
    int m_retrievals_this_iteration, m_current_retrieval, m_stage_number;
    QString m_status_msg_template;
    // librdf_query *m_query_forum_posts_by_type,
    //   *m_query_forum_posts_by_container_of,
    //   *m_query_forum_posts_by_has_container,
    //   *m_query_forum_posts_by_has_reply, *m_query_forum_posts_by_reply_of,
    //   *m_query_forum_posts_by_reply_to, *m_query_users, *m_query_post_alldata;
    // librdf_model *m_model;
    QString m_sparql_forum_posts_by_type,
      m_sparql_forum_posts_by_container_of,
      m_sparql_forum_posts_by_has_container,
      m_sparql_forum_posts_by_has_reply, m_sparql_forum_posts_by_reply_of,
      m_sparql_forum_posts_by_reply_to, m_sparql_users, m_sparql_post_alldata;
    const Soprano::Backend *m_be;
    Soprano::Model *m_model;
    const Soprano::Parser *m_parser;
    SIOCForumMessage *m_selected_message;
};

#endif
