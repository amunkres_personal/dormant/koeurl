// siocforummessage.cpp
// Copyright (C) 2008-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#include <ktemporaryfile.h>
#include <kio/job.h>
#include <kselectaction.h>
#include <kactioncollection.h>
#include <klocale.h>
#include <QTextStream>
#include "siocforummessage.h"
#include "siocforumcommon.h"

#include "siocforummessage.moc" // This must be here for moc to be run.

// SIOCForumMessage::SIOCForumMessage(librdf_query_results *r):
SIOCForumMessage::SIOCForumMessage(const Soprano::QueryResultIterator &r):
  m_data(r)
{
  m_data.next();
}

const QString SIOCForumMessage::getMIMEType() const
{
  return "text/plain";
}

void SIOCForumMessage::buildPartsMenu(KMenu *menu,
                                      KActionCollection *actions) const
{
  // TODO:For now, this will present a SIOC forum message as having only one
  // part, which is of type text/plain. Maybe later, have it be able to present
  // other things as parts, e.g. embedded HTML or links to URLs.
  
  // KRadioAction *a = new KRadioAction(i18n("&Content"), 0, this,
  //                                    SLOT(doNothing()),
  //                                    actions,"msgpart_only");
  // a->setChecked(true);
  KSelectAction *sa(actions->add<KSelectAction>("msgparts"));
  sa->setText(i18n("&Parts"));
  KAction *a(sa->addAction(i18n("&Content")));
  connect(a, SIGNAL(triggered()), this, SLOT(doNothing()));
  // // TODO: The "exclusive group" for any instance of a Message subclass should
  // // be exclusive to itself. Suggestion: convert "this" pointer to a
  // // hexadecimal string, and use that as the exclusive group.
  // a->setExclusiveGroup("temp_group_string");
  menu->addAction(sa);
}

void SIOCForumMessage::doNothing()
{
  // If/when this message type supports multiple message parts, this slot will
  // be used to change which part is selected.
}

void SIOCForumMessage::beginLoadingPartData(KParts::ReadOnlyPart *viewer)
{
  QString content(getLiteralValue(m_data, 5));
  if (viewer->openStream(getMIMEType(), KUrl()))
  {
    // The KPart supports {open,write,close}Stream.
    viewer->writeStream(content.toLocal8Bit());
    viewer->closeStream();
  }
  else
  {
    // The KPart does not support {open,write,close}Stream. In this case, we
    // need to use a temporary file?
    KTemporaryFile temp;
    temp.open();
    QTextStream(&temp) << content;
    KUrl temp_url(temp.fileName());
    temp.close();
    viewer->openUrl(temp_url);
    connect(viewer, SIGNAL(completed()), this, SLOT(deleteTempFile()));
    m_viewer = viewer;
  }
}

void SIOCForumMessage::deleteTempFile()
{
  disconnect(m_viewer, SIGNAL(completed()), 0, 0);
  KIO::file_delete(m_viewer->url(), false);
}
