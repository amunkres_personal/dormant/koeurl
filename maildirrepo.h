// maildirrepo.h
// Copyright (C) 2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#ifndef _MAILDIRREPO_H
#define _MAILDIRREPO_H

#include <kdirlister.h>
#include <kurl.h>
#include <QString>
#include <QDomElement>
#include <QDomDocument>
#include "msgrepo.h"

class MaildirRepo: public MsgRepo
{
  Q_OBJECT
  public:
    MaildirRepo(const MaildirRepoType *type, const QDomElement &config);
  private:
    void constructorCommon();
    KUrl m_path;
};

#endif _MAILDIRREPO_H
