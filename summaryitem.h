// summaryitem.h
// Copyright (C) 2008 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#ifndef _SUMMARYITEM_H
#define _SUMMARYITEM_H

#include <QTreeWidget>

class TopPane;

class SummaryItem: public  QTreeWidgetItem
{
  public:
    SummaryItem(TopPane *summary_view, const QString &id);
    SummaryItem(SummaryItem *parent_item, const QString &id);
    const QString &getID() const;
  private:
    QString m_id;
};

#endif
