#include <librdf.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
  librdf_world *w;
  librdf_storage *s;
  librdf_model *m;
  librdf_query *q;
  librdf_query_results *r;
  librdf_uri *uri;
  
  /* Initialize model object (and its dependencies) */
  w = librdf_new_world();
  librdf_world_open(w);
  s = librdf_new_storage(w, "hashes", "jimbo", "hash-type='memory'");
  m = librdf_new_model(w, s, NULL);
  
  /* Load data into the model (main index) */
  uri = librdf_new_uri(w, argv[1]);
  if (librdf_model_load(m, uri, NULL, NULL, NULL))
  {
    fprintf(stderr, "Could not load URL");
    exit(1);
  }
  librdf_free_uri(uri);
  
  /* Load message data */
  q = librdf_new_query(w, "sparql", NULL,
                   "PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
                   "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                   "\n"
                   "SELECT ?post ?postId ?postSeeAlso\n"
                   "WHERE\n"
                   "{\n"
                   "  ?x sioc:container_of ?post.\n"
                   "  OPTIONAL\n"
                   "  {\n"
                   "    ?post sioc:id ?postId.\n"
                   "    ?post rdfs:seeAlso ?postSeeAlso.\n"
                   "  }\n"
                   "}\n", NULL);
  r = librdf_query_execute(q, m);
  do
  {
    librdf_node *result;
    
    if (!librdf_query_results_get_binding_value(r, 1))
    {
      result = librdf_query_results_get_binding_value(r, 2);
      if (!result)
      {
        result = librdf_query_results_get_binding_value(r, 0);
      }
      /* Note: librdf docs say to copy URI before using it */
      uri = librdf_new_uri_from_uri(librdf_node_get_uri(result));
      librdf_model_load(m, uri, NULL, NULL, NULL);
      librdf_free_uri(uri);
    }
  } while (!librdf_query_results_next(r));
  librdf_free_query_results(r);
  librdf_free_query(q);
  
  /* Load subscribers */
  q = librdf_new_query(w, "sparql", NULL,
                       "PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
                       "\n"
                       "SELECT ?subscriber ?email_sha1sum\n"
                       "WHERE\n"
                       "{\n"
                       "  ?x sioc:has_subscriber ?subscriber.\n"
                       "  OPTIONAL { ?subscriber sioc:email_sha1sum "
                       "?email_sha1sum. }\n"
                       "}\n", NULL);
  r = librdf_query_execute(q, m);
  do
  {
    librdf_node *subscriber;
    
    if (!librdf_query_results_get_binding_value(r, 1))
    {
      subscriber = librdf_query_results_get_binding_value(r, 0);
      uri = librdf_new_uri_from_uri(librdf_node_get_uri(subscriber));
      librdf_model_load(m, uri, NULL, NULL, NULL);
      librdf_free_uri(uri);
    }
  } while (!librdf_query_results_next(r));
  librdf_free_query_results(r);
  librdf_free_query(q);
  
  /* Query for post contents */
  q = librdf_new_query(w, "sparql", NULL,
                       "PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
                       "PREFIX rdf: "
                       "<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                       "PREFIX dc: <http://purl.org/dc/elements/1.1/>\n"
                       "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
                       "\n"
                       "SELECT ?post ?postTitle ?date ?user ?userName "
                       "?content ?parent ?list ?listName\n"
                       "WHERE\n"
                       "{\n"
                       "  ?post rdf:type sioc:Post.\n"
                       "  ?post dc:title ?postTitle.\n"
                       "  ?post dcterms:created ?date.\n"
                       "  ?post sioc:content ?content.\n"
                       "  ?post sioc:has_creator ?user.\n"
                       "  ?user sioc:name ?userName.\n"
                       "  ?post sioc:has_container ?list.\n"
                       "  ?list dc:title ?listName.\n"
                       "  OPTIONAL { ?post sioc:reply_of ?parent. }\n"
                       "}\n"
                       "ORDER BY ?date\n", NULL);
  r = librdf_query_execute(q, m);
  do
  {
    librdf_node *post_n, *username_n, *user_n, *listname_n, *list_n,
                *posttitle_n, *date_n, *parent_n;
    librdf_uri *post_u, *user_u, *list_u, *parent_u;
    unsigned char *post_s, *username_s, *user_s, *listname_s, *list_s,
                  *posttitle_s, *date_s, *parent_s;
    
    post_n = librdf_query_results_get_binding_value(r, 0);
    post_u = librdf_node_get_uri(post_n);
    post_s = librdf_uri_as_string(post_u);
    username_n = librdf_query_results_get_binding_value(r, 4);
    username_s = librdf_node_get_literal_value(username_n);
    user_n = librdf_query_results_get_binding_value(r, 3);
    user_u = librdf_node_get_uri(user_n);
    user_s = librdf_uri_as_string(user_u);
    listname_n = librdf_query_results_get_binding_value(r, 8);
    listname_s = librdf_node_get_literal_value(listname_n);
    list_n = librdf_query_results_get_binding_value(r, 7);
    list_u = librdf_node_get_uri(list_n);
    list_s = librdf_uri_as_string(list_u);
    posttitle_n = librdf_query_results_get_binding_value(r, 1);
    posttitle_s = librdf_node_get_literal_value(posttitle_n);
    date_n = librdf_query_results_get_binding_value(r, 2);
    date_s = librdf_node_get_literal_value(date_n);
    
    printf("Post URI: %s\n"
           "From: %s <%s>\n"
           "To: %s <%s>\n"
           "Subject: %s\n"
           "Date: %s\n", post_s, username_s, user_s, listname_s, list_s,
           posttitle_s, date_s);
    parent_n = librdf_query_results_get_binding_value(r, 6);
    if (parent_n)
    {
      parent_u = librdf_node_get_uri(parent_n);
      parent_s = librdf_uri_as_string(parent_u);
      printf("Parent: %s\n\n", parent_s);
    }
    else
    {
      printf("\n");
    }
  } while (!librdf_query_results_next(r));
}
