// msgrepo.h
// Copyright (C) 2007-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#ifndef _MSGREPO_H
#define _MSGREPO_H

#include <QObject>
#include <QString>
#include <QVector>
#include <QDomElement>
#include "message.h"

class KDE_EXPORT MsgRepoIterator
{
  public:
    virtual ~MsgRepoIterator();
    virtual void operator++() = 0;
    virtual bool operator!=(const MsgRepoIterator &) const = 0;
    virtual QString getID() const = 0;
    virtual QString getParentID() const = 0;
    virtual QString getField(int index) const = 0;
};

class KDE_EXPORT MsgRepo: public QObject
{
  Q_OBJECT
  public:
    typedef MsgRepoIterator iterator;
    virtual QString name() const = 0;
    virtual void summaryFields(QStringList &) const = 0;
    virtual QDomElement createConfig(QDomDocument) const = 0;
    virtual void beginActivatingRepo(KMenu *, KActionCollection *) = 0;
    virtual void beginActivatingMessage(const QString &) = 0;
    virtual iterator *begin() = 0;
    virtual iterator *end() = 0;
    virtual Message *getSelectedMessage() const = 0;
  signals:
    void readyToShow();
    void readyToShowMessage();
    void statusMessage(const QString &, int timeout = 0);
    void progressRange(int, int);
    void progressValue(int);
    void progressVisible(bool);
};

#endif
