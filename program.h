// program.h
// Copyright (C) 2007-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#ifndef _PROGRAM_H
#define _PROGRAM_H

#include <kxmlguiwindow.h>
#include <kio/job.h>
#include <kaction.h>
#include <kparts/part.h>
#include <kstatusbar.h>
#include <QByteArray>
#include <QDomDocument>
#include <QMap>
#include <QFrame>
#include <QTreeWidget>
#include <QSplitter>
#include <QProgressBar>
#include "msgrepo.h"
#include "leftpane.h"
#include "msgrepoitem.h"
#include "msgrepotype.h"

class ProgramWindow: public KXmlGuiWindow
{
  Q_OBJECT
  public:
    ProgramWindow();
    virtual ~ProgramWindow();
    void insertRepo(MsgRepo *repo);
    void saveRepoLayout(const char *onFinishWrite = 0, const char *onFinishNoWrite = 0);
  public slots:
    void showRepoMenu(QTreeWidgetItem *item, const QPoint &p);
    void selectRepo(QTreeWidgetItem *repo_item);
    void selectMessage(QTreeWidgetItem *msg_item);
    void showMessage();
    void showMessagePart();
  protected:
    virtual void closeEvent(QCloseEvent *event);
  private slots:
    void finishedReading(KJob *done_j);
    void foldersText(KIO::Job *, QByteArray &data);
    void deleteChosenRepo();
    void horizontalSplitterMoved(int, int);
    void verticalSplitterMoved(int, int);
    void beginShutdown();
    void quitProgram(KJob *);
    void quitProgram();
  private:
    void rewriteFolders(const char *onFinishWrite = 0);
    QSplitter *m_hsplit, *m_vsplit;
    LeftPane *m_lp;
    TopPane *m_tp;
    QFrame *m_holder;
    QWidget *m_bp;
    KParts::ReadOnlyPart *m_bp_part;
    KAction *m_del;
    ReadBuffer *m_inbuf;
    QByteArray *m_outbuf;
    MsgRepoItem *m_active_repo;
    typedef QMap<QString, MsgRepoType *> MsgRepoTypeMap;
    MsgRepoTypeMap m_repotypes;
    QDomDocument m_folders;
    Message *m_displayed_message;
    bool m_ready_to_close;
    KStatusBar *m_status_bar;
    QProgressBar *m_progress_bar;
};

#endif
