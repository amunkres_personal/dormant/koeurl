#ifndef _MYTREEWIDGET_H
#define _MYTREEWIDGET_H

#include <QTreeWidget>
#include <QTreeWidgetItem>

class MyTreeWidget: public QTreeWidget
{
  Q_OBJECT
  public:
    MyTreeWidget(QWidget *parent);
  signals:
    void selectionChanged(QTreeWidgetItem *);
  private slots:
    void selectionChangeNotify();
};

#endif
