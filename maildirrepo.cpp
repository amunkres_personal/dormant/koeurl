// maildirrepo.cpp
// Copyright (C) 2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#include "maildirrepo.h"

#include "common.h" // This must be here for moc to be run.

/*
MaildirRepo::MaildirRepo(const )
{
}
*/

MaildirRepo::MaildirRepo(const MaildirRepoType *type,
                         const QDomElement &config):
  m_type(type), m_name(config.attribute("name")),
  m_path(config.attribute("path"))
{
  constructorCommon();
}

void MaildirRepo::constructorCommon()
{
  m_lister = new KDirLister();
}


void MaildirRepo::beginActivatingRepo(KMenu *menu,
                                      KActionCollection *actions)
{
  // NOTE: this method is supposed to be called once each time that the
  // repository is selected by the user.
  // Create actions for this repository here.
  
  // TODO: not sure what actions, if any, a maildir repo should have
  // There should be "Create new folder" and "Delete folder", at least.
  // "Create new folder" action
  KAction *a(actions->addAction(i18n("&Create new folder")));
  a->setIcon(KIcon("folder"));
  connect(a, SIGNAL(triggered()), this, SLOT(createFolder()));
  menu->addAction(a);
  // "Delete folder" action
  a = actions->addAction(i18n("&Delete folder"));
  a->setIcon(KIcon("user-trash"));
  connect(a, SIGNAL(triggered()), this, SLOT(deleteFolder()));
  menu->addAction(a);
  
  // Here, build the "summary" of the repo. (In the SIOC repo plugin, this
  // part is in a separate method "beginBuildingSummary", and we might want to
  // create such a method here eventually).
  
  // Scan "new" and "cur" subdirectories of the maildir for messages.
  // TODO: use KDirLister for this, with autoUpdate enabled
  connect(m_lister, SIGNAL(newItems(const KFileItemList &)), this, SLOT(filesFound(const KFileItemList &)));
  connect(m_lister, SIGNAL(deleteItem(const KFileItem &)), this, SLOT(fileDeleted(const KFileItem &)));
  KUrl url(m_path);
  // Scan for folders
  m_lister->openUrl(url);
  // Scan for new messages and existing messages
  url.cd("new");
  m_lister->openUrl(url);
  url.cd("../cur");
  m_lister->openUrl(url, KDirLister::Keep);
}

void MaildirRepo::filesFound(const KFileItemList &files)
{
  KFileItemList::const_iterator iter(files.constBegin()), end(files.constEnd());

  // kDebug() << "Got a batch of new items.";
  for (; end != iter; ++iter)
  {
    // kDebug() << "New directory entry:" << (*iter).url().prettyUrl();
    
    // First, check if the found "file" is actually a folder. It's a folder is these conditions are met:
    // - It must be a directory.
    // - It must be a direct child of the top-level Maildir (that is, m_path).
    // - Its filename must begin with "." but must not be exactly "." or "..".
    KUrl parent_dir((*iter).url().upUrl());
    QString name((*iter).url().fileName());
    if ((*iter).isDir() && (parent_dir == m_path))
    {
      if ((name.size() > 1) && (name[0] == QChar('.')) && (name[1] != QChar('.')))
      {
        // It's a folder, so we must add it to the scan.
        m_lister->openUrl((*iter).url(), KDirLister::Keep);
        // Wait, do we really want to do that? For any folder other than the currently-selected one, we will have no place to put the SummaryItems.
        // Suggestion: switch SummaryItem over to using the model/view system
        // We also should create an entry in the left pane for this folder, as a child of the entry for the repository.
        // TODO: implement this (probably requires API changes)
      }
    }
    
    // It's not a folder, so check whether it's a message. Conditions:
    // - It must be a file (not a directory).
    // - Its parent directory must be named "new" or "cur".
    // - Its parent directory must not be the top-level Maildir.
    // - It must not have a name beginning with a ".".
    QString parent_dir_name(parent_dir.fileName());
    if ((*iter).isFile() && ((parent_dir_name == "new") || (parent_dir_name == "cur"))
        && (parent_dir != m_path) && (name[0] != QChar('.')))
    {
      // It is a message. Now, we must find out:
      // - Is it in a "new" directory or a "cur" directory?
      // - Is it in a folder or the top-level Maildir?
      // - If it's in a folder, what folder?
      if (parent_dir_name == "new")
      {
        // In this case, all we do is move it into "cur", appending the info flags to its name (probably it should begin with no info flags present, indicated by ":2,") when we do that. This means that we will see the newly-created directory entry eventually, and will process the message for real then.
        // TODO: implement this
      }
      else // parent_dir_name == "cur"
      {
        KUrl grandparent_dir(parent_dir.upUrl());
        // The "grandparent_dir" identifies the folder to which the message belongs.
        // Now we need to put create a SummaryItem for the mesage in the "model" for its folder.
        // TODO: rework TopPane / SummaryItem to use the model/view system, then implement this.
        // (also need to handle the "info" part of filenames; this can wait)
      }
    }
  }
}
