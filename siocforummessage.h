// siocforummessage.h
// Copyright (C) 2008-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#ifndef _SIOCFORUMMESSAGE_H
#define _SIOCFORUMMESSAGE_H

// #include <librdf.h>
#include <kparts/part.h>
#include <Soprano/QueryResultIterator>
#include "message.h"

class SIOCForumMessage: public Message
{
  Q_OBJECT
  public:
    // SIOCForumMessage(librdf_query_results *r);
    SIOCForumMessage(const Soprano::QueryResultIterator &r);
    virtual const QString getMIMEType() const;
    virtual void buildPartsMenu(KMenu *menu,
                                KActionCollection *actions) const;
    virtual void beginLoadingPartData(KParts::ReadOnlyPart *viewer);
  private slots:
    void doNothing();
    void deleteTempFile();
  private:
    // librdf_query_results *m_data;
    Soprano::QueryResultIterator m_data;
    KParts::ReadOnlyPart *m_viewer;
};

#endif
