# Koeurl: a SIOC discussion-reader app for KDE Plasma desktop

(**Warning**: I haven't touched this project since December 2009.)

Koeurl is a user agent program for threaded discussions. It aims to support multiple different types of threaded discussion systems through the use of plugins. Currently, the only plugin that exists is for [SIOC](http://sioc-project.org/).

The program depends on version 4 of the [KDE](http://www.kde.org/) libraries. The SIOC plugin also requires the [Soprano](http://soprano.sourceforge.net/) RDF meta-framework and one of its backends, [Sesame](http://www.openrdf.org/) or [Redland](http://librdf.org/). (Currently, it is hardcoded to use Sesame, due to problems I encountered when using Redland. It may be made configurable later.)

Some links to related topics (be warned, these are likely to be far out of date):
- [Semantically-Interlinked Online Communities (SIOC)](http://sioc-project.org/)
- [Buxon (another SIOC user agent) and SWAML (a SIOC-oriented mailing list archiver similar to Mhonarc or Pipermail)](http://swaml.berlios.de/)
- [Sesame RDF framework](http://www.openrdf.org/)
- [Redland RDF Libraries](http://librdf.org/)
- [Soprano RDF meta-framework for KDE](http://soprano.sourceforge.net/)
- [KDE Plasma desktop environment](https://www.kde.org/)
