// readbuffer.cpp
// Copyright (C) 2007-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#include <qbuffer.h>
#include "readbuffer.h"

#include "readbuffer.moc" // This must be here for moc to be run.

ReadBuffer::ReadBuffer()
{
  m_qbuf = new QBuffer();
  m_qbuf->open(QIODevice::WriteOnly);
}

ReadBuffer::~ReadBuffer()
{
  delete m_qbuf;
}

void ReadBuffer::appendData(KIO::Job *, const QByteArray &data)
{
  // this is based on FileRetriever::slotData() in Akregator
  m_qbuf->write(data);
}
