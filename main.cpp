// main.cpp
// Copyright (C) 2007-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#include <kapplication.h>
#include <kcmdlineargs.h>
#include <klocale.h>
#include <kaboutdata.h>
#include "program.h"
#include "common.h"

extern "C" KDE_EXPORT int kdemain(int argc, char **argv)
{
  KAboutData *aboutdata =
    new KAboutData("koeurl", QByteArray(), ki18n("Koeurl"),
                   KOEURL_VERSION_STRING, ki18n("Threaded discussion client"),
                   KAboutData::License_GPL,
                   ki18n("(C) 2007-2009 Andrew Munkres"));
  
  KCmdLineArgs::init(argc, argv, aboutdata);
  KApplication a;
  ProgramWindow *p = new ProgramWindow();
  p->show();
  
  return a.exec();
}
