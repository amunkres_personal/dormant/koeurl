// siocforumrepo.cpp
// Copyright (C) 2007-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#include <ktemporaryfile.h>
#include <kdebug.h>
#include <klocale.h>
#include <kactioncollection.h>
#include <Soprano/Global>
#include <Soprano/PluginManager>
#include <Soprano/StorageModel>
#include <Soprano/StatementIterator>
#include <Soprano/Error/Error>
#include "siocforumrepo.h"
#include "siocforumcommon.h"
#include "common.h"

#include "siocforumrepo.moc" // This must be here for moc to be run.

SIOCForumRepo::SIOCForumRepo(const SIOCForumRepoType *type,
                             const QString &name,
                             const QString &uri):
  m_type(type), m_name(name), m_uri(uri)
{
  constructorCommon();
}

SIOCForumRepo::SIOCForumRepo(const SIOCForumRepoType *type,
                             const QDomElement &config):
  m_type(type), m_name(config.attribute("name")),
  m_uri(config.attribute("uri"))
{
  constructorCommon();
}

void SIOCForumRepo::constructorCommon()
{
  /*
  m_world = librdf_new_world();
  librdf_world_open(m_world);
  m_storage = 0;
  */
  m_retrievals_this_iteration = 0;
  m_stage_number = 0;
  m_initialized = false;
  // Redland seems to have bugs; try Sesame2 for now
  m_be = Soprano::discoverBackendByName("sesame2");
  if (0 == m_be)
  {
    kError() << i18n("Couldn't open Sesame2 Soprano backend.") << endl;
    return;
  }
  m_model = 0;
  m_parser = Soprano::PluginManager::instance()
               ->discoverParserForSerialization(Soprano::SerializationRdfXml);
  
  /* As of version 1.0.7, Redland does not support UNION in queries, so can't
     use this query:
  m_sparql_forum_posts =
              "PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
              "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
              "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
              "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
              "\n"
              "SELECT ?post ?postSeeAlso\n"
              "WHERE\n"
              "{\n"
              "  {\n"
              "    { ?post rdf:type sioc:Post. }\n"
              "    UNION\n"
              "    { ?forum sioc:container_of ?post. }\n"
              "    UNION\n"
              "    { ?post sioc:has_container ?forum. }\n"
              "    UNION\n"
              "    { ?parent sioc:has_reply ?post. }\n"
              "    UNION\n"
              "    { ?post sioc:reply_to ?parent. }\n"
              "  }\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post dcterms:created ?date.\n"
              "    ?post rdfs:seeAlso ?postSeeAlso.\n"
              "  }.\n"
              "  FILTER (!BOUND(?date))\n"
              "}\n";
  m_query_forum_posts = librdf_new_query(m_world, "sparql", NULL,
                                         sparql_forum_posts, NULL);
  */
  // NOTE: the "?x ?y ?z." stuff is a workaround for bug 000077 in Redland.
  // http://bugs.librdf.org/mantis/view.php?id=77
  m_sparql_forum_posts_by_type =
              "PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
              "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
              "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
              "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
              "\n"
              "SELECT ?post ?postSeeAlso\n"
              "WHERE\n"
              "{\n"
              "  ?post rdf:type sioc:Post.\n"
              "  ?x ?y ?z.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post dcterms:created ?date.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post rdfs:seeAlso ?postSeeAlso.\n"
              "  }.\n"
              "  FILTER (!BOUND(?date) && ?x = ?post\n"
              "          && ?y = rdf:type && ?z = sioc:Post)\n"
              "}\n";
  /*
  m_query_forum_posts_by_type = librdf_new_query(m_world, "sparql", NULL,
                                                 sparql_forum_posts_by_type,
                                                 NULL);
  */
  m_sparql_forum_posts_by_container_of =
              "PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
              "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
              "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
              "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
              "\n"
              "SELECT ?post ?postSeeAlso\n"
              "WHERE\n"
              "{\n"
              "  ?forum sioc:container_of ?post.\n"
              "  ?x ?y ?z.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post dcterms:created ?date.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post rdfs:seeAlso ?postSeeAlso.\n"
              "  }.\n"
              "  FILTER (!BOUND(?date) && ?x = ?forum\n"
              "          && ?y = sioc:container_of && ?z = ?post)\n"
              "}\n";
  /*
  m_query_forum_posts_by_container_of =
    librdf_new_query(m_world, "sparql", NULL,
                     sparql_forum_posts_by_container_of, NULL);
  */
  m_sparql_forum_posts_by_has_container =
              "PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
              "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
              "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
              "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
              "\n"
              "SELECT ?post ?postSeeAlso\n"
              "WHERE\n"
              "{\n"
              "  ?post sioc:has_container ?forum.\n"
              "  ?x ?y ?z.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post dcterms:created ?date.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post rdfs:seeAlso ?postSeeAlso.\n"
              "  }.\n"
              "  FILTER (!BOUND(?date) && ?x = ?post\n"
              "          && ?y = sioc:has_container && ?z = ?forum)\n"
              "}\n";
  /*
  m_query_forum_posts_by_has_container =
    librdf_new_query(m_world, "sparql", NULL,
                     sparql_forum_posts_by_has_container, NULL);
  */
  m_sparql_forum_posts_by_has_reply =
              "PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
              "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
              "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
              "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
              "\n"
              "SELECT ?post ?postSeeAlso\n"
              "WHERE\n"
              "{\n"
              "  ?parent sioc:has_reply ?post.\n"
              "  ?x ?y ?z.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post dcterms:created ?date.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post rdfs:seeAlso ?postSeeAlso.\n"
              "  }.\n"
              "  FILTER (!BOUND(?date) && ?x = ?parent\n"
              "          && ?y = sioc:has_reply && ?z = ?post)\n"
              "}\n";
  /*
  m_query_forum_posts_by_has_reply =
    librdf_new_query(m_world, "sparql", NULL,
                     sparql_forum_posts_by_has_reply, NULL);
  */
  m_sparql_forum_posts_by_reply_of =
              "PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
              "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
              "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
              "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
              "\n"
              "SELECT ?post ?postSeeAlso\n"
              "WHERE\n"
              "{\n"
              "  ?post sioc:reply_of ?parent.\n"
              "  ?x ?y ?z.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post dcterms:created ?date.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post rdfs:seeAlso ?postSeeAlso.\n"
              "  }.\n"
              "  FILTER (!BOUND(?date) && ?x = ?post\n"
              "          && ?y = sioc:reply_of && ?z = ?parent)\n"
              "}\n";
  /*
  m_query_forum_posts_by_reply_of =
    librdf_new_query(m_world, "sparql", NULL,
                     sparql_forum_posts_by_reply_of, NULL);
  */
  // The reply_to predicate is nonstandard, but WordPress uses it instead of
  // reply_of. Grrrrr...
  m_sparql_forum_posts_by_reply_to =
              "PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
              "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
              "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
              "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
              "\n"
              "SELECT ?post ?postSeeAlso\n"
              "WHERE\n"
              "{\n"
              "  ?post sioc:reply_to ?parent.\n"
              "  ?x ?y ?z.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post dcterms:created ?date.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post rdfs:seeAlso ?postSeeAlso.\n"
              "  }.\n"
              "  FILTER (!BOUND(?date) && ?x = ?post\n"
              "          && ?y = sioc:reply_to && ?z = ?parent)\n"
              "}\n";
  /*
  m_query_forum_posts_by_reply_to =
    librdf_new_query(m_world, "sparql", NULL,
                     sparql_forum_posts_by_reply_to, NULL);
  */
  m_sparql_users =
                   "PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
                   "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                   "\n"
                   "SELECT ?user ?userSeeAlso\n"
                   "WHERE\n"
                   "{\n"
                   "  ?post sioc:has_creator ?user.\n"
                   "  ?x ?y ?z.\n"
                   "  OPTIONAL\n"
                   "  {\n"
                   "    ?user sioc:name ?userName.\n"
                   "  }.\n"
                   "  OPTIONAL\n"
                   "  {\n"
                   "    ?user rdfs:seeAlso ?userSeeAlso.\n"
                   "  }.\n"
                   "  FILTER (!BOUND(?date) && ?x = ?post\n"
                   "          && ?y = sioc:has_creator && ?z = ?user)\n"
                   "}\n";
  /*
  m_query_users = librdf_new_query(m_world, "sparql", NULL, sparql_users,
                                   NULL);
  */
  m_sparql_post_alldata =
              "PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
              "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
              "PREFIX dc: <http://purl.org/dc/elements/1.1/>\n"
              "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
              "\n"
              "SELECT ?post ?postTitle ?date ?user ?userName ?userWithoutName "
              "?content ?parentByReplyOf ?parentByReplyTo ?parentByHasReply "
              "?list ?listName ?listWithoutName\n"
              "WHERE\n"
              "{\n"
              "  ?post rdf:type sioc:Post.\n"
              "  ?post dcterms:created ?date.\n"
              "  ?x ?y ?z.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post dc:title ?postTitle.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post sioc:content ?content.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post sioc:has_creator ?user.\n"
              "    ?user sioc:name ?userName.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post sioc:has_creator ?userWithoutName.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post sioc:has_container ?list.\n"
              "    ?list dc:title ?listName.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post sioc:has_container ?listWithoutName.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post sioc:reply_of ?parentByReplyOf.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?post sioc:reply_to ?parentByReplyTo.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?parentByHasReply sioc:has_reply ?post.\n"
              "  }.\n"
              "  FILTER (?x = ?post && ?y = rdf:type && ?z = sioc:Post)\n"
              "}\n";
  /*
  m_query_post_alldata = librdf_new_query(m_world, "sparql", NULL,
                                          sparql_post_alldata, NULL);
  */
  
  // NOTE: KIO seems to ask for text/html by default; some servers
  // (swaml.berlios.de in particular) seem to offer both HTML and RDF/XML on
  // the same URLs, so we should request that it *not* send us HTML
  m_headers["accept"] = "*/*, text/html; q=0.0, application/xhtml+xml; q=0.0";
  m_headers["User-Agent"] = "Koeurl/" KOEURL_VERSION_STRING;
  
  m_initialized = true;
}

QString SIOCForumRepo::name() const
{
  return m_name;
}

void SIOCForumRepo::summaryFields(QStringList &fields) const
{
  fields.append(i18n("From"));
  fields.append(i18n("Subject"));
  fields.append(i18n("Date"));
}

QDomElement SIOCForumRepo::createConfig(QDomDocument doc) const
{
  QDomElement config(doc.createElement("folder"));
  config.setAttribute("type", m_type->m_libname);
  config.setAttribute("name", m_name);
  config.setAttribute("uri", m_uri);
  return config;
}

void SIOCForumRepo::beginActivatingRepo(KMenu *menu,
                                        KActionCollection *actions)
{
  // NOTE: this method is supposed to be called once each time that the
  // repository is selected by the user.
  // Create actions for this repository here. 
  KAction *a(actions->addAction(i18n("&Reload")));
  a->setIcon(KIcon("view-refresh"));
  connect(a, SIGNAL(triggered()), this, SLOT(reload()));
  menu->addAction(a);
  
  beginBuildingSummary();
}

void SIOCForumRepo::reload()
{
  delete m_model;
  m_model = 0;
  beginBuildingSummary();
}

void SIOCForumRepo::beginBuildingSummary()
{
  // if (m_storage) // message data already loaded, use it
  if (m_model) // message data already loaded, use it
  {
    emit readyToShow();
  }
  else // need to load message data
  {
    /*
    char storage_name[] = "hashes", name[] = "jimbo",
      options_string[] = "hash-type='memory'";
    m_storage = librdf_new_storage(m_world, storage_name, name,
                                   options_string);
    m_model = librdf_new_model(m_world, m_storage, NULL);
    */
    // TODO: maybe should add here a setting whether to use disk or memory
    // storage
    m_model = m_be->createModel();
    loadIndex();
  }
}

void SIOCForumRepo::loadIndex()
{
  // Retrieve the message index
  // KTempFile temp(QString::null, ".rdf");
  // KUrl temp_url(KUrl::fromPathOrURL(temp.name()));
  // temp.close();
  // kDebug() << i18n("Retrieving message index URL %1").arg(m_uri) << endl;
  // emit statusMessage(i18n("Retrieving message index URL %1").arg(m_uri), 0);
  // TODO: how to handle status messages with the retrieval queue mechanism?
  // KIO::FileCopyJob *j = KIO::file_copy(m_uri, temp_url, -1, true, false,
  //                                      false);
  // connect(j, SIGNAL(result(KIO::Job *)),
  //         this, SLOT(processIndex(KIO::Job *)));
  // NOTE: instead of the above, use the retrieval queue mechanism for this:
  enqueueUnvisitedURL(m_uri);
  disconnect(this, SIGNAL(proceed()), 0, 0);
  connect(this, SIGNAL(proceed()), this, SLOT(retrieveMessages()));
  m_status_msg_template =
    i18n("Loading SIOC graph entry point (retrieving URL %1 of %2)");
  beginQueuedRetrievals();
}

// bool SIOCForumRepo::loadIntoModel(KUrl local_url)
bool SIOCForumRepo::loadIntoModel(KUrl local_url, KUrl orig_url)
{
  // Populate the "model" with the message index
  kDebug() << i18n("Loading temporary file %1 into model")
                .arg(local_url.url()) << endl;
  // librdf_uri *uri(librdf_new_uri(m_world,
  //                                static_cast<unsigned char *>
  //                                  (static_cast<void *>
  //                                    (const_cast<char *>
  //                                      (static_cast<const char *>
  //                                        (local_url.url().local8Bit()))))));
  // bool success(!librdf_model_load(m_model, uri, NULL, NULL, NULL));
  // librdf_free_uri(uri);
  
  // NOTE: it appears that "parseFile" requires a "baseUri" parameter, and
  // this should be the original URL of the RDF/XML data.
  // NOTE: some trace statements here for debugging
  // Soprano::Error::ErrorCode error(
  //   m_model->addStatements((m_parser->parseFile(local_url.path(), orig_url,
  //                                               Soprano::SerializationRdfXml))
  //                             .allStatements()));
  QList<Soprano::Statement> s
    (m_parser->parseFile(local_url.path(), orig_url,
                         Soprano::SerializationRdfXml).allStatements());
  kDebug() << i18n("Found %1 statements in the file").arg(s.size()) << endl;
  Soprano::Error::ErrorCode error(m_model->addStatements(s));
  kDebug() << i18n("Currently there are %1 statements in the model").arg(m_model->statementCount()) << endl;
  
  KIO::file_delete(local_url, KIO::HideProgressInfo);
  return (Soprano::Error::ErrorNone == error);
}

/*
void SIOCForumRepo::processIndex(KIO::Job *copied)
{
  if (0 != copied->error())
  {
    // Which way should it display the error message? This way:
    // copied->showErrorDialog();
    // Or this way:
    kError() << copied->errorString() << endl;
  }
  // else if (!loadIntoModel((static_cast<KIO::FileCopyJob *>(copied))
  //                            ->destURL()))
  // NOTE: loadIntoModel now takes the original URL as 2nd parameter
  else if (!loadIntoModel((static_cast<KIO::FileCopyJob *>(copied))
                             ->destURL(),
                          (static_cast<KIO::FileCopyJob *>(copied))
                             ->srcURL()))
  {
    kError() << i18n("Could not load the data retrieved from the main URL"
                      " %1 into the RDF model.")
                   .arg(static_cast<KIO::FileCopyJob *>(copied)
                          ->srcURL().url()) << endl;
    return;
  }
  retrieveMessages();
}
*/

void SIOCForumRepo::retrieveMessages()
{
  // Retrieve message data
  // TODO: identify different sioc:Forum objects and present them to the
  // user, possibly listing the contents separately. Also do this with
  // sioc:Site objects. This will require changes to the repository API.
  
  // Remove the queues of URLs from the previous iteration.
  m_host_queues.clear();
  
  // It uses 5 different queries to identify post nodes which need to have more
  // data retrieved. (If librdf supported the UNION keyword, it would be able
  // to accomplish this with only 1 query.)
  bool found_unvisited(retrieveMessagesBy(m_sparql_forum_posts_by_type));
  found_unvisited =
    retrieveMessagesBy(m_sparql_forum_posts_by_container_of) || found_unvisited;
  found_unvisited =
    retrieveMessagesBy(m_sparql_forum_posts_by_has_container) ||
      found_unvisited;
  found_unvisited =
    retrieveMessagesBy(m_sparql_forum_posts_by_has_reply) || found_unvisited;
  found_unvisited =
    retrieveMessagesBy(m_sparql_forum_posts_by_reply_of) || found_unvisited;
  found_unvisited =
    retrieveMessagesBy(m_sparql_forum_posts_by_reply_to) || found_unvisited;
  
  disconnect(this, SIGNAL(proceed()), 0, 0);
  if (found_unvisited)
  {
    // When at least one unvisited URL was visited in this iteration, we
    // must begin another iteration.
    ++m_stage_number;
    connect(this, SIGNAL(proceed()), this, SLOT(retrieveMessages()));
    m_status_msg_template =
      i18n("Loading extra message data, stage %1 (retrieving URL %2 of %3)")
        .arg(m_stage_number);
    beginQueuedRetrievals();
  }
  else
  {
    // When no unvisisted URLs were found in the results of a single
    // execution of the query, this method/query won't be executed again.
    // In this case, it's time to advance to the next step, which is
    // loading user data.
    retrieveUserData();
  }
  // Now, wait for the data to be retrieved and loaded into the model. We
  // know it's finished when m_current_retrieval becomes equal to
  // m_retrievals_this_iteration.
}

// bool SIOCForumRepo::retrieveMessagesBy(librdf_query *query)
bool SIOCForumRepo::retrieveMessagesBy(const QString &query)
{
  // Retrieve messages using a single query.
  // librdf_node *result;
  Soprano::Node result;
  // librdf_uri *uri;
  bool found_unvisited(false);
  // librdf_query_results *r(librdf_query_execute(query, m_model));
  // NOTE: some trace statements here for debugging
  Soprano::QueryResultIterator
    r(m_model->executeQuery(query, Soprano::Query::QueryLanguageSparql));
  kDebug() << i18n("A query to find not-yet-retrieved URIs with message data found %1 binding sets").arg(r.allBindings().size()) << endl;
  r = m_model->executeQuery(query, Soprano::Query::QueryLanguageSparql);
  // NOTE: Soprano documentation shows calling "next" on an iterator before
  // retrieving the first row of bindings, so presumably doing so is necessary
  // and won't skip the first bindings...
  // if (!librdf_query_results_finished(r))
  // {
  //   do
  while (r.next())
  {
    // TODO: put in the new query
    // If this post has an rdfs:seeAlso, use the referenced URI as the
    // URL to be retrieved. Otherwise, use the URI of the post node
    // itself as the URL to be retrieved.
    // result = librdf_query_results_get_binding_value(r, 1);
    // if (!result)
    // {
    //   result = librdf_query_results_get_binding_value(r, 0);
    // }
    result = r.binding(1);
    // TODO: I'm not sure what Soprano would return for OPTIONAL variables that
    // aren't bound; would it be an "empty" node, or an "invalid" node?
    if (!result.isResource())
    {
      result = r.binding(0);
    }
    
    // // Note: librdf docs for librdf_node_get_uri say to copy the
    // // returned URI before using it. I don't know if what I'm doing
    // // with it here counts as "using", but I'll copy it just in case.
    // uri = librdf_new_uri_from_uri(librdf_node_get_uri(result));
    // found_unvisited =
    //  (enqueueUnvisitedURL(
    //   static_cast<const char *>(
    //       static_cast<void *>(librdf_uri_as_string(uri))))
    //   || found_unvisited);
    // librdf_free_uri(uri);
    // librdf_free_node(result);
    found_unvisited = (enqueueUnvisitedURL(result.uri()) || found_unvisited);
  //   } while (!librdf_query_results_next(r));
  }
  // librdf_free_query_results(r);
  return found_unvisited;
}

bool SIOCForumRepo::enqueueUnvisitedURL(KUrl src_url)
{
  URLMap::const_iterator iter_end(m_visited_urls.end());
  // Strip the "reference" part of the URL
  src_url.setRef(QString::null);
  // Load data from the URL *only if* it hasn't been loaded before in
  // this session. This is to avoid hammering the server with lots of
  // HTTP requests. (I'm not sure I can rely on the KIO HTTP cache for
  // this purpose.)
  // if ((static_cast<const URLMap>(m_visited_urls)).find(src_url.url()) ==
  //     iter_end)
  if (m_visited_urls.find(src_url.url()) == iter_end)
  {
    m_visited_urls.insert(src_url.url(), 0);
    QString host(src_url.host());
    HostMap::iterator host_iter(m_host_queues.find(host));
    if (m_host_queues.end() == host_iter)
    {
      host_iter = m_host_queues.insert(host, URLQueue());
    }
    (*host_iter).append(src_url);
    
    // Note: we increment the counter here, knowing that the result
    // signal is a "delayed" signal (it can't be raised until we get
    // back to the main loop). Thus, the counter can't return to zero
    // until all retrievals finish.
    ++m_retrievals_this_iteration;
    // kDebug() << i18n("Enqueued a URL; there are now %1 URLs to be retrieved")
    //               .arg(m_remaining_retrievals) << endl;
    return true;
  }
  else
  {
    // kDebug() << i18n("Not retrieving additional data URL %1 because it has"
    //                   " already been retrieved").arg(src_url.url()) << endl;
    return false;
  }
}

void SIOCForumRepo::beginQueuedRetrievals()
{
  // kDebug() << i18n("Beginning queued retrievals") << endl;
  m_current_retrieval = 0;
  emit progressRange(0, m_retrievals_this_iteration);
  // Begin retrieving the first URL in the queue for each host; it will
  // proceed to the subsequent URL for each host when the current ones
  // finish.
  URLQueue::iterator url_iter;
  HostMap::iterator host_iter(m_host_queues.begin()),
    host_iter_end(m_host_queues.end());
  for (; host_iter_end != host_iter; ++host_iter)
  {
    retrieveQueuedURL(*host_iter);
  }
  emit progressVisible(true); // ensure it's reset to 0 before showing it
}

void SIOCForumRepo::retrieveQueuedURL(URLQueue &queue)
{
  KTemporaryFile temp;
  temp.setSuffix(".rdf");
  // NOTE: it seems that we need to open the file to retrieve its name
  temp.open();
  KUrl temp_url(temp.fileName());
  temp.close();
  KUrl src_url(queue.first());
  queue.pop_front();
  // kDebug() << i18n("Retrieving additional data URL %1 to temporary location"
  //                  " %2").arg(src_url.url(), temp_url.url()) << endl;
  emit statusMessage(m_status_msg_template
                       .arg(m_current_retrieval + 1)
                         .arg(m_retrievals_this_iteration));
  emit progressValue(m_current_retrieval);
  KIO::FileCopyJob *j(KIO::file_copy(src_url, temp_url, -1,
                                     KIO::Overwrite | KIO::HideProgressInfo));
  j->addMetaData(m_headers);
  connect(j, SIGNAL(result(KJob *)),
          this, SLOT(retrievalFinished(KJob *)));
}

void SIOCForumRepo::retrievalFinished(KJob *copied)
{
  if (0 != copied->error())
  {
    // Which way should it display the error message? This way:
    // copied->showErrorDialog();
    // Or this way:
    kError() << copied->errorString() << endl;
  }
  // NOTE: loadIntoModel now takes the original URL as 2nd parameter
  else if (!loadIntoModel((static_cast<KIO::FileCopyJob *>(copied))
                             ->destUrl(),
                          (static_cast<KIO::FileCopyJob *>(copied))
                             ->srcUrl()))
  {
    kError() << i18n("Could not load the data retrieved from URL %1 into the "
                     "RDF model.")
                   .arg(static_cast<KIO::FileCopyJob *>(copied)
                          ->srcUrl().url()) << endl;
  }
  // Should there be any attempt to retry automatically in the case of error?
  // For now, there won't be.
  
  // --m_remaining_retrievals;
  // kDebug() << i18n("There are %1 URLs remaining to be retrieved")
  //               .arg(m_remaining_retrievals) << endl;
  ++m_current_retrieval;
  kDebug() << i18n("Finished retrieving URL %1 out of %2")
                .arg(m_current_retrieval).arg(m_retrievals_this_iteration)
           << endl;
  // if (0 >= m_remaining_retrievals)
  if (m_current_retrieval >= m_retrievals_this_iteration)
  {
    // This stage of data retrieval has finished. Reset the counter.
    m_retrievals_this_iteration = 0;
    
    // Do what needs to be done next.
    emit progressVisible(false);
    emit statusMessage("");
    emit proceed();
  }
  else
  {
    // Find the queue of URLs for the host of this URL, and begin retrieving
    // the next one from the queue.
    HostMap::iterator
      host_iter(m_host_queues.find(static_cast<KIO::FileCopyJob *>(copied)
                                     ->srcUrl().host()));
    // Note: it should *always* find the host in m_host_queues, correct? (What
    // if HTTP redirect occurred? Would the value returned by srcUrl() be
    // different than what was originally requested? I hope not!)
    if (!((*host_iter).isEmpty()))
    {
      retrieveQueuedURL(*host_iter);
    }
  }
}

void SIOCForumRepo::retrieveUserData()
{
  disconnect(this, SIGNAL(proceed()), 0, 0);
  
  // Again, wait for the data to be retrieved and loaded into the model.
  // When it's finished, notify the main program that it is ready.
  connect(this, SIGNAL(proceed()), this, SIGNAL(readyToShow()));
  
  // Reset the counter of message data retrieval iterations.
  m_stage_number = 0;
  
  // Retrieve user data
  // librdf_query_results *r(librdf_query_execute(m_query_users, m_model));
  // NOTE: some trace statements here for debugging
  Soprano::QueryResultIterator
    r(m_model->executeQuery(m_sparql_users,
                            Soprano::Query::QueryLanguageSparql));
  kDebug() << i18n("Query to find URIs with user data found %1 binding sets").arg(r.allBindings().size()) << endl;
  r = m_model->executeQuery(m_sparql_users,
                            Soprano::Query::QueryLanguageSparql);
  // librdf_node *result;
  Soprano::Node result;
  // librdf_uri *uri;
  // if (!librdf_query_results_finished(r))
  // {
  bool results_found(false);
  // do
  while (r.next())
  {
    if (!results_found)
    {
      // Before processing the first URL, remove the queues of URLs from the
      // previous iteration.
      m_host_queues.clear();
      results_found = true;
    }
    // Similar to the previous queries, load extra user data only if a
    // sioc:name for the user isn't present. Load it from the rdfs:seeAlso
    // URI if it is present, otherwise load it from the user node's own
    // URI.
    // result = librdf_query_results_get_binding_value(r, 1);
    // if (!result)
    // {
    //   result = librdf_query_results_get_binding_value(r, 0);
    // }
    result = r.binding(1);
    // TODO: I'm not sure what Soprano would return for OPTIONAL variables that
    // aren't bound; would it be an "empty" node, or an "invalid" node?
    if (!result.isResource())
    {
      result = r.binding(0);
    }
    // uri = librdf_new_uri_from_uri(librdf_node_get_uri(result));
    // enqueueUnvisitedURL(
    //   static_cast<const char *>(
    //     static_cast<void *>(librdf_uri_as_string(uri))));
    // librdf_free_uri(uri);
    // librdf_free_node(result);
    enqueueUnvisitedURL(result.uri());
  // } while (!librdf_query_results_next(r));
  }
  
  if (results_found)
  {
    m_status_msg_template =
      i18n("Loading extra user data (retrieving URL %1 of %2");
    beginQueuedRetrievals();
  }
  else
  {
    // If this query found no results, then the retrieval of RDF data is
    // already finished.
    emit proceed();
  }
  // librdf_free_query_results(r);
}

MsgRepo::iterator *SIOCForumRepo::begin()
{
  // librdf_query_results *r(librdf_query_execute(m_query_post_alldata,
  //                                              m_model));
  // return static_cast<MsgRepo::iterator *>(new SIOCForumRepoIterator(r));
  
  // NOTE: some trace statements here for debugging
  // return static_cast<MsgRepo::iterator *>(
  //   new SIOCForumRepoIterator(
  //     m_model->executeQuery(m_sparql_post_alldata,
  //                           Soprano::Query::QueryLanguageSparql)));
  Soprano::QueryResultIterator r(
    m_model->executeQuery(m_sparql_post_alldata,
                          Soprano::Query::QueryLanguageSparql));
  kDebug() << i18n("Query to populate a repo iterator (m_sparql_post_alldata) found %1 binding sets").arg(r.allBindings().size()) << endl;
  r = m_model->executeQuery(m_sparql_post_alldata,
                            Soprano::Query::QueryLanguageSparql);
  return static_cast<MsgRepo::iterator *>(new SIOCForumRepoIterator(r));
}

MsgRepo::iterator *SIOCForumRepo::end()
{
  return static_cast<MsgRepo::iterator *>(new SIOCForumRepoIterator());
}

void SIOCForumRepo::beginActivatingMessage(const QString &id)
{
  // Note: currently, the value of id was returned by
  // SIOCForumRepoIterator::getURIValue below, and therefore it should not
  // contain illegal characters (particularly < and >, which could cause a
  // SIOC injection vulnerability), correct? In that case, no escaping needs
  // to be done on id. (However, maybe later there will be a way to provide an
  // arbitrary id.)
  QString sparql_entire_post_by_id_template(
      QString("PREFIX sioc: <http://rdfs.org/sioc/ns#>\n"
              "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
              "PREFIX dc: <http://purl.org/dc/elements/1.1/>\n"
              "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
              "\n"
              "SELECT ?postTitle ?date ?user ?userName ?userWithoutName "
              "?content ?parentByReplyTo ?parentByHasReply ?list ?listName "
              "?listWithoutName\n"
              "WHERE\n"
              "{\n"
              "  <%1> rdf:type sioc:Post.\n"
              "  <%1> dcterms:created ?date.\n"
              "  ?x ?y ?z.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    <%1> dc:title ?postTitle.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    <%1> sioc:content ?content.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    <%1> sioc:has_creator ?user.\n"
              "    ?user sioc:name ?userName.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    <%1> sioc:has_creator ?userWithoutName.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    <%1> sioc:has_container ?list.\n"
              "    ?list dc:title ?listName.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    <%1> sioc:has_container ?listWithoutName.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    <%1> sioc:reply_of ?parentByReplyOf.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    <%1> sioc:reply_to ?parentByReplyTo.\n"
              "  }.\n"
              "  OPTIONAL\n"
              "  {\n"
              "    ?parentByHasReply sioc:has_reply <%1>.\n"
              "  }.\n"
              "  FILTER (?x = <%1> && ?y = rdf:type && ?z = sioc:Post)\n"
              "}\n").replace("%1", id));
  // librdf_query_results *r(
  //   librdf_query_execute(
  //     librdf_new_query(m_world, "sparql", NULL,
  //                      static_cast<const unsigned char *>
  //                        (static_cast<const void *>
  //                          (sparql_entire_post_by_id_template)), NULL),
  //                      m_model));
  // m_selected_message = new SIOCForumMessage(r);
  // NOTE: some trace statements here for debugging
  Soprano::QueryResultIterator r(
    m_model->executeQuery(sparql_entire_post_by_id_template,
                          Soprano::Query::QueryLanguageSparql));
  kDebug() << i18n("Query to extract contents of a post found %1 binding sets")
                .arg(r.allBindings().size()) << endl;
  m_selected_message =
    new SIOCForumMessage(
      m_model->executeQuery(sparql_entire_post_by_id_template,
                            Soprano::Query::QueryLanguageSparql));
  emit readyToShowMessage();
}

Message *SIOCForumRepo::getSelectedMessage() const
{
  return m_selected_message;
}
// SIOCForumRepoIterator::SIOCForumRepoIterator(librdf_query_results *r):
SIOCForumRepoIterator::SIOCForumRepoIterator(
  const Soprano::QueryResultIterator &r): m_results(r)
{
  // NOTE: Soprano documentation shows calling "next" on an iterator before
  // retrieving the first row of bindings, so presumably doing so is necessary
  // and won't skip the first bindings...
  m_count = (m_results.next() ? 0 : -1);
}

SIOCForumRepoIterator::SIOCForumRepoIterator():
//  m_count(-1), m_results(0)
  m_count(-1), m_results()
{
}

void SIOCForumRepoIterator::operator++()
{
  // if ((-1 == m_count) || librdf_query_results_next(m_results))
  if ((-1 == m_count) || !m_results.next())
  {
    m_count = -1;
  }
  else
  {
    ++m_count;
  }
  // NOTE: a typical operator++() would do "return *this;", but this one
  // doesn't, since we don't need to use the return value, and because I'm
  // unsure about the type issues it raises (e.g. if it is declared as
  // returning MsgRepoIterator, isn't that illegal because MsgRepoIterator
  // is pure virtual?)
}

bool SIOCForumRepoIterator::operator!=(const MsgRepo::iterator &other) const
{
  return (m_count !=
    (static_cast<const SIOCForumRepoIterator *>(&other))->m_count);
}

QString SIOCForumRepoIterator::getID() const
{
  return getURIValue(m_results, 0).toString();
}

QString SIOCForumRepoIterator::getParentID() const
{
  // Three different predicates, "reply_of", "reply_to" (nonstandard), and
  // "has_reply" may be used to find the reply.
  QUrl result(getURIValue(m_results, 7));
  if (result.isEmpty())
  {
    result = getURIValue(m_results, 8);
  }
  if (result.isEmpty())
  {
    result = getURIValue(m_results, 9);
  }
  // kDebug() << "parentId() is returning value " << result << endl;
  return result.toString();
}

QString SIOCForumRepoIterator::getField(int index) const
{
  switch (index)
  {
    case 0: // "From" (user linked to the post by sioc:has_creator)
      return getConditionalCompositeValue(m_results, 4, 3, 5);
    case 1: // "Subject" (string linked to the post by dc:title)
      return getLiteralValue(m_results, 1);
    case 2: // "Date" (string linked to the post by dcterms:created)
      return getLiteralValue(m_results, 2);
    default: // error
      return QString::null;
  }
}
