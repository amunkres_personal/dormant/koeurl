// siocforumrepodialog.cpp
// Copyright (C) 2007-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#include <klocale.h>
#include <qlayout.h>
#include <qlabel.h>
#include "siocforumrepodialog.h"

SIOCForumRepoDialog::SIOCForumRepoDialog(QWidget *parent): KDialog(parent)
{
  setCaption(i18n("Configure SIOC Forum Repository"));
  QFrame *f = new QFrame(this);
  setMainWidget(f);
  QGridLayout *g_l = new QGridLayout(f);
  // It used to set "margin" to 0 in the constructor; do we want to do that?
  g_l->setSpacing(5);
  QLabel *name = new QLabel(i18n("Name"), f);
  g_l->addWidget(name, 0, 0, Qt::AlignLeft);
  m_name = new KLineEdit(f);
  g_l->addWidget(m_name, 0, 1, Qt::AlignLeft);
  QLabel *uri = new QLabel(i18n("URI"), f);
  g_l->addWidget(uri, 1, 0, Qt::AlignLeft);
  m_uri = new KLineEdit(f);
  g_l->addWidget(m_uri, 1, 1, Qt::AlignLeft);
  
  connect(this, SIGNAL(finished()), this, SLOT(delayedDestruct()));
}

SIOCForumRepoDialog::~SIOCForumRepoDialog()
{
  delete m_name;
  delete m_uri;
}
