// leftpane.h
// Copyright (C) 2007 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#ifndef _LEFTPANE_H
#define _LEFTPANE_H

#include <QSplitter>
#include "mytreewidget.h"
#include "msgrepoitem.h"

class ReadBuffer;

class LeftPane: public MyTreeWidget
{
  Q_OBJECT
  public:
    LeftPane(QSplitter *parent);
    virtual ~LeftPane();
  signals:
    void contextMenu(QTreeWidgetItem *, const QPoint &);
    void repoSelected(MsgRepoItem *);
  public slots:
    void toggleShown(bool);
  protected:
    virtual void contextMenuEvent(QContextMenuEvent *event);
  private:
    bool m_shown;
};

#endif
