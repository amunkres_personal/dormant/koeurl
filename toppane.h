// toppane.h
// Copyright (C) 2007-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#ifndef _TOPPANE_H
#define _TOPPANE_H

#include <QSplitter>
#include "mytreewidget.h"

class TopPane: public MyTreeWidget
{
  Q_OBJECT
  public:
    TopPane(QSplitter *parent);
  public slots:
    void toggleShown(bool);
    void performSort(int column = -1);
  private:
    bool m_shown;
};

#endif
