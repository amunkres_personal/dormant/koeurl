// siocforumcommon.cpp
// Copyright (C) 2008-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

// #include <librdf.h>
#include "siocforumcommon.h"
#include <Soprano/Node>

QUrl getURIValue(const Soprano::QueryResultIterator &results, int index)
{
  // librdf_node *n(librdf_query_results_get_binding_value(results, index));
  // if (!n)
  Soprano::Node n(results.binding(index));
  if (!n.isResource())
  {
    return QUrl();
  }
  // librdf_uri *u(librdf_new_uri_from_uri(librdf_node_get_uri(n)));
  // QString value(QString::fromUtf8(
  //                 static_cast<const char *>(
  //                   static_cast<void *>(librdf_uri_as_string(u)))));
  // librdf_free_uri(u);
  // librdf_free_node(n);
  // return value;
  return n.uri();
}

QString getLiteralValue(const Soprano::QueryResultIterator &results,
                        int index)
{
  // librdf_node *n(librdf_query_results_get_binding_value(results, index));
  // if (!n)
  Soprano::Node n(results.binding(index));
  if (!n.isLiteral())
  {
    return QString::null;
  }
  // QString value(QString::fromUtf8(
  //                 static_cast<const char *>(
  //                   static_cast<void *>(librdf_node_get_literal_value(n)))));
  // librdf_free_node(n);
  // return value;
  return n.literal().toString();
}

// Currently, nothing uses this function:
// QString getCompositeValue(librdf_query_results *results, int l_index,
//                           int u_index)
// {
//   return getLiteralValue(results, l_index) +
//          " <" + getURIValue(results, u_index) + ">";
// }

QString getConditionalCompositeValue(const Soprano::QueryResultIterator &results,
                                     int l_index, int u_index_1,
                                     int u_index_2)
{
  QUrl uri(getURIValue(results, u_index_1));
  if (uri.isEmpty())
  {
    uri = getURIValue(results, u_index_2);
  }
  QString literal(getLiteralValue(results, l_index));
  return literal + " <" + uri.toString() + ">";
}
