// siocforumrepotype.h
// Copyright (C) 2007-2009 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#ifndef _SIOCFORUMREPOTYPE_H
#define _SIOCFORUMREPOTYPE_H

#include <qstring.h>
#include <qdom.h>
#include "msgrepotype.h"
#include "program.h"
#include "siocforumrepodialog.h"
#include "msgrepo.h"

class SIOCForumRepoType: public MsgRepoType
{
  Q_OBJECT
  public:
    SIOCForumRepoType(QObject *parent, const QStringList &args);
    const QString m_libname;
    virtual MsgRepo *createRepo(const QDomElement &) const;
  public slots:
    void configureNewRepo();
    void createRepo();
  private:
    ProgramWindow *m_parent;
    SIOCForumRepoDialog *m_dlg;
};

#endif
