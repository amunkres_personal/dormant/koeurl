#include "mytreewidget.h"

#include "mytreewidget.moc" // This must be here for moc to be run.

MyTreeWidget::MyTreeWidget(QWidget *parent): QTreeWidget(parent)
{
  connect(this, SIGNAL(itemSelectionChanged()),
          this, SLOT(selectionChangeNotify()));
}

void MyTreeWidget::selectionChangeNotify()
{
  QList<QTreeWidgetItem *> si(selectedItems());
  QTreeWidgetItem *item(0);
  if (0 != si.size())
    item = si.first();
  emit selectionChanged(item);
}
